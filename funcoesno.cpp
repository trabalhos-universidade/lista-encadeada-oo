#include "funcoesno.h"

no::no() {
}

no::no(int cod,string nome,int id) {
    this->cod = cod;
    this->nome = nome;
    this->id = id;
    prox = NULL;
}

no::~no() {
    //cout<<endl<<"Removido: ";
    this->consulta();
}

void no::setcod(int cod) {
    this->cod = cod;
}

void no::setnome(string nome) {
    this->nome = nome;
}

void no::setid(int id) {
    this->id = id;
}

void no::setprox(no *prox) {
    this->prox = prox;
}

no *no::getprox() {
    return prox;
}

int no::getcod() {
    return cod;
}

string no::getnome() {
    return nome;
}

int no::getid() {
    return id;
}

void no::consulta() {
    cout<<endl<<"Codigo: "<<cod;
    cout<<endl<<"Nome: "<<nome;
    cout<<endl<<"Idade: "<<id;
}

