#include "funcoesno.h"

class list {
    private:
        no *l;
        
    public:
        list();
        ~list();
        no *get();
        void set(no *l);
        void insere(int cod,string nome,int id);
        void remove(int cod);
        void listatodos();
        no *consultacod(int cod);
        no *consultanome(string nome);
        void edita(no *aux, int cod, string nome, int id);
        bool listavazia();
};
