#include "funcoes.h"

class no {
    private:
        int cod;
        string nome;
        int id;
        no *prox;

    public:
        no();
        no(int cod, string nome, int id);
        ~no();
        int getcod();
        void setcod(int cod);
        string getnome();
        void setnome(string nome);
        int getid();
        void setid(int id);
        no *getprox();
        void setprox(no *prox);
        void consulta();
};
