#include "funcoeslist.h"

list::list() {
    l = NULL;
}

list::~list() {
    no *aux;
    while(l!=NULL) {
        aux = l;
        l = l->getprox();
        delete (aux);
    }
}

bool list::listavazia() {
    if (l == NULL)
        return true;
    else
        return false;
}

void list::listatodos() {
    no *aux = l;
    while(aux!=NULL) {
        cout<<endl<<"Codigo: "<<aux->getcod();
        cout<<endl<<"Nome: "<<aux->getnome();
        cout<<endl<<"Idade: "<<aux->getid()<<endl;
        aux = aux->getprox();
    }
}

void list::set(no *l) {
    this->l = l;
}

no *list::get() {
    return l;
}

void list::insere(int cod, string nome, int id) {
    no *aux = new no(cod,nome,id);
    no *ult;
    if(l==NULL)
        l = aux;
    else {
        ult=l;
        while(ult->getprox()!=NULL)
            ult = ult->getprox();
        ult->setprox(aux);
    }
}

void list::remove(int cod) {
    no *aux;
    no *ult;
    if (cod == l->getcod()) {
        ult = l;
        l = l->getprox();
    }
    else {
        aux = l;
        ult = l->getprox();
        while((ult != NULL) && (cod != ult->getcod())) {
            aux = ult;
            ult = ult->getprox();
        }
        aux->setprox(ult->getprox());
    }
    delete ult;
}

no *list::consultanome(string nome) {
    no *aux;
    aux = l;
    while((aux!=NULL) && (nome!=aux->getnome()))
        aux = aux->getprox();
    return aux;
}

no *list::consultacod(int cod) {
    no *aux;
    aux = l;
    while((aux!=NULL) && (cod!=aux->getcod()))
        aux = aux->getprox();
    return aux;
}

void list::edita(no *aux, int cod, string nome, int id) {
    aux->setcod(cod);
    aux->setnome(nome);
    aux->setid(id);
}
