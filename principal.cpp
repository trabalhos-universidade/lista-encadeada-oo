#include "funcoeslist.h"

int menu();

main() {
    list *lista = new list;
    int op,id,cod;
    string nome;
    no *aux;
    do {
        system("cls");
        cout<<endl<<"   Lista encadeada OO"<<endl<<endl;
        op=menu();
        switch(op) {
            case 1: cout<<endl<<"Reiniciando lista"<<endl;
                    system("pause");
                    delete lista;
                    lista = new list;
                    break;
                    
            case 2: cout<<endl<<"Inserindo"<<endl;
                    cout<<endl<<"Codigo: ";
                    cin>>cod;
                    cout<<endl<<"Nome: ";
                    cin>>nome;
                    cout<<endl<<"Idade: ";
                    cin>>id;
                    lista->insere(cod,nome,id);
                    break;
                    
            case 3: if (!lista->listavazia()) {
                        cout<<endl<<"Removendo"<<endl;
                        cout<<endl<<"Codigo: ";
                        cin>>cod;
                        aux = lista->consultacod(cod);
                        if (aux!=NULL)
                            lista->remove(cod);
                        else
                            cout<<endl<<"Codigo invalido."<<endl;
                    }
                    else
                        cout<<endl<<"Lista vazia"<<endl;
                    system("pause");
                    break;
                    
            case 4: if (!lista->listavazia()) {
                        cout<<endl<<"Consultando por codigo"<<endl;
                        cout<<endl<<"Codigo: ";
                        cin>>cod;
                        aux = lista->consultacod(cod);
                        if (aux!=NULL)
                            aux->consulta();
                        else
                            cout<<endl<<"Codigo invalido."<<endl;
                    }
                    else
                        cout<<endl<<"Lista vazia"<<endl;
                    system("pause");
                    break;
            
            case 5: if (!lista->listavazia()) {
                        cout<<endl<<"Consultando por nome"<<endl;
                        cout<<endl<<"Nome: ";
                        cin>>nome;
                        aux = lista->consultanome(nome);
                        if (aux!=NULL)
                            aux->consulta();
                        else
                            cout<<endl<<"Nome inexistente."<<endl;
                    }
                    else
                        cout<<endl<<"Lista vazia"<<endl;
                    system("pause");
                    break;
                    
            case 6: if (!lista->listavazia()) {
                        cout<<endl<<"Listando todos"<<endl;
                        lista->listatodos();
                    }
                    else
                        cout<<endl<<"Lista vazia"<<endl;
                    system("pause");
                    break;
                    
            case 7: if (!lista->listavazia()) {
                        cout<<endl<<"Editando"<<endl;
                        cout<<endl<<"Codigo: ";
                        cin>>cod;
                        aux = lista->consultacod(cod);
                        if (aux!=NULL) {
                            aux->consulta();
                            cout<<endl<<"Novo codigo: ";
                            cin>>cod;
                            cout<<endl<<"Novo nome: ";
                            cin>>nome;
                            cout<<endl<<"Nova idade: ";
                            cin>>id;
                            lista->edita(aux,cod,nome,id);
                        }
                        else
                            cout<<endl<<"Codigo invalido."<<endl;
                    }
                    else
                        cout<<endl<<"Lista vazia"<<endl;
                    system("pause");
                    break;
        }
    }while (op!=0);
}

int menu() {
    int op=0;
    cout<<endl<<"1 - Reiniciar lista";
    cout<<endl<<"2 - Inserir";
    cout<<endl<<"3 - Remover";
    cout<<endl<<"4 - Consultar especifico - pelo codigo";
    cout<<endl<<"5 - Consultar especifico - pelo nome";
    cout<<endl<<"6 - Consultar todos";
    cout<<endl<<"7 - Editar";
    cout<<endl<<"0 - Sair"<<endl;
    cout<<endl<<"Digite a opcao: ";
    cin>>op;
    return op;
}
